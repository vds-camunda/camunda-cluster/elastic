FROM elasticsearch:7.14.2

HEALTHCHECK --interval=30s --timeout=5s --retries=3 CMD curl -f http://localhost:9200/_cat/health | grep -q green || exit 1